# Experimental Berlin Image Processing Service
Experimental Berlin's service for processing images. For every processed image, a thumbnail
of appropriate dimensions is produced.

## Testing
```$ go test```