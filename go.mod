module gitlab.com/experimental-berlin/image-processing

go 1.13

require (
	github.com/disintegration/imaging v1.5.0
	golang.org/x/image v0.0.0-20180920235150-e1a1ede6891c // indirect
	golang.org/x/net v0.0.0-20180921000356-2f5d2388922f // indirect
)
